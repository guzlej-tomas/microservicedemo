package cz.xitee.dbag;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CollateralService {

	@RequestMapping(path ="/collateral", method = RequestMethod.GET)
	public Long getCollateralAmmount() {
		return 25L;		
	}
}
