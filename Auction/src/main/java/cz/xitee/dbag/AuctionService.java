package cz.xitee.dbag;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/")
public class AuctionService {

	@Value("${custom.openshift.hostname}")
	private String hostname;

	@Autowired
	RestTemplate restTemplate;

	private List<String> auctions = new ArrayList<String>();

	@RequestMapping(path = "/createAuction", method = RequestMethod.POST)
	public ResponseEntity<Void> createAuction(HttpServletRequest request) {
		auctions.add(auctions.size() + "");
		return ResponseEntity.ok().build();
	}

	@RequestMapping(path = "/getAuctions", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getAuctions(HttpServletRequest request) {
		return new ResponseEntity<List<String>>(this.auctions, HttpStatus.OK);
	}

	@RequestMapping(path = "/collateralRequest", method = RequestMethod.GET)
	public ResponseEntity<Long> sendCollateralRequest() {
		Long result = restTemplate.getForObject("http://collateral." + hostname + ".svc.cluster.local:8070/collateral",
				Long.class);
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}

	@RequestMapping(path = "/settlementRequest", method = RequestMethod.GET)
	public ResponseEntity<List<String>> sendSettlementRequest() {
		@SuppressWarnings("unchecked")
		List<String> results = restTemplate
				.getForObject("http://settlement." + hostname + ".svc.cluster.local:8071/settlements", List.class);
		return new ResponseEntity<List<String>>(results, HttpStatus.OK);
	}

	@RequestMapping(path = "/callOtherServices", method = RequestMethod.GET)
	public ResponseEntity<String> callOtherServices() {
		@SuppressWarnings("unchecked")
		List<String> results = restTemplate
				.getForObject("http://settlement." + hostname + ".svc.cluster.local:8071/settlements", List.class);
		Long result = restTemplate.getForObject("http://collateral." + hostname + ".svc.cluster.local:8070/collateral",
				Long.class);
		return new ResponseEntity<String>(results.toString() + "@@" + result.toString(), HttpStatus.OK);
	}

}
