package cz.xitee.dbag;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SettlementService {
	
	@RequestMapping(path="/settlement", method = RequestMethod.POST)
	public ResponseEntity<Void> createSettlement(HttpServletRequest request) {
		System.out.println(request.getParameterMap());
		return new ResponseEntity<Void>(HttpStatus.OK);	
	}
	@RequestMapping(path="/settlements", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getSettlements(HttpServletRequest request) {
		System.out.println(request.getParameterMap());
		List<String> settlements = new ArrayList<String>();
		settlements.add("settlement1");
		settlements.add("settlement2");
		return new ResponseEntity<List<String>>(settlements, HttpStatus.OK);	
	}
}
